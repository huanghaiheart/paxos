package priv.pfz.paxos.role;

import com.google.common.collect.Maps;
import lombok.Getter;
import lombok.Setter;
import priv.pfz.paxos.common.ToString;

import java.util.Map;

/**
 * @author pengfangzhou
 * @date 2022/2/4 22:08
 */
@Getter
@Setter
public class AcceptorInfo extends ToString {

    private final Map<String, Acceptor> acceptorMap = Maps.newHashMap();
    private int total;
    private int majority;
}
