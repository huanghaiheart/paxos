package priv.pfz.paxos.role;

/**
 * @author pengfangzhou
 * @date 2022/2/14 2:30
 */
public interface Events {
    String PREPARE_REQ = "prepareReq";
    String PREPARE_RESP = "prepareResp";
    String ACCEPT_REQ = "acceptReq";
    String ACCEPT_RESP = "acceptResp";
}
