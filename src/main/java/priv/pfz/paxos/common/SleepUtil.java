package priv.pfz.paxos.common;

import lombok.extern.slf4j.Slf4j;

/**
 * @author pengfangzhou
 * @date 2022/2/4 16:27
 */
@Slf4j
public class SleepUtil {

    public static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (Exception e) {
            log.info(e.getMessage(), e);
        }
    }
}
