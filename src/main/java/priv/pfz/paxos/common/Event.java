package priv.pfz.paxos.common;

import lombok.Getter;
import lombok.Setter;

import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

import static java.util.concurrent.TimeUnit.NANOSECONDS;

/**
 * @author pengfangzhou
 * @date 2022/2/6 19:00
 */
@Getter
@Setter
public class Event implements Delayed {
    private final Runnable runnable;
    private final long effectTime;

    public Event(Runnable runnable, long effectTime) {
        this.runnable = runnable;
        this.effectTime = effectTime;
    }

    @Override
    public long getDelay(TimeUnit unit) {
        return unit.convert(effectTime - System.nanoTime(), NANOSECONDS);
    }

    @Override
    public int compareTo(Delayed other) {
        if (other == this) // compare zero if same object
            return 0;
        if (other instanceof Event) {
            Event x = (Event) other;
            long diff = effectTime - x.effectTime;
            if (diff < 0)
                return -1;
            else if (diff > 0)
                return 1;
            else
                return 1;
        }
        long diff = getDelay(NANOSECONDS) - other.getDelay(NANOSECONDS);
        return (diff < 0) ? -1 : (diff > 0) ? 1 : 0;
    }
}
