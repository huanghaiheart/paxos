package priv.pfz.paxos;

import lombok.extern.slf4j.Slf4j;
import priv.pfz.paxos.role.PaxosCluster;

/**
 * Hello world!
 *
 */
@Slf4j
public class App {

    public static void main( String[] args ) {
        System.out.println( "Hello Paxos!" );
        PaxosCluster paxosCluster = new PaxosCluster();
        while (true) {
            paxosCluster.run();
        }
    }
}
