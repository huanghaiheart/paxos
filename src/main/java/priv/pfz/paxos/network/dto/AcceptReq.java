package priv.pfz.paxos.network.dto;

import lombok.Getter;
import lombok.Setter;
import priv.pfz.paxos.common.ToString;

/**
 * @author pengfangzhou
 * @date 2022/2/3 18:11
 */
@Getter
@Setter
public class AcceptReq extends ToString {
    private int proposalId;
    private String value;
}
